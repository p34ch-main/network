# Network tool

![image](./doc/image.png "Interface")

This is a simple application to send or get netword data. You can use it as starting point in your experiments. Supports TCP and UDP server/client.

- Simple entity
  - Just connection
- Log entity
  - Allows you to see data
  - Also allows to send data
- Echo entity (server only)
  - Responce with incoming message
  - You can send custom message to the client

## Libraries

- [ASIO](https://github.com/chriskohlhoff/asio.git)
- [GLFW](https://github.com/glfw/glfw.git)
- [Glad](https://glad.dav1d.de/)
- [ImGUI](https://github.com/ocornut/imgui.git)
- [spdlog](https://github.com/gabime/spdlog.git)


## SAST Tools

[PVS-Studio](https://pvs-studio.com/en/pvs-studio/?utm_source=website&utm_medium=github&utm_campaign=open_source) - static analyzer for C, C++, C#, and Java code.
