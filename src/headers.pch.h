#pragma once

#include <algorithm>     // IWYU pragma: export
#include <array>         // IWYU pragma: export
#include <chrono>        // IWYU pragma: export
#include <fstream>       // IWYU pragma: export
#include <future>        // IWYU pragma: export
#include <list>          // IWYU pragma: export
#include <map>           // IWYU pragma: export
#include <memory>        // IWYU pragma: export
#include <mutex>         // IWYU pragma: export
#include <queue>         // IWYU pragma: export
#include <set>           // IWYU pragma: export
#include <span>          // IWYU pragma: export
#include <string>        // IWYU pragma: export
#include <string_view>   // IWYU pragma: export
#include <thread>        // IWYU pragma: export
#include <type_traits>   // IWYU pragma: export
#include <unordered_map> // IWYU pragma: export
#include <unordered_set> // IWYU pragma: export
#include <utility>       // IWYU pragma: export

#include <asio.hpp>                       // IWYU pragma: export
#include <spdlog/sinks/basic_file_sink.h> // IWYU pragma: export
#include <spdlog/spdlog.h>                // IWYU pragma: export
#include <spdlog/stopwatch.h>             // IWYU pragma: export
#include <imgui.h>                        // IWYU pragma: export
#include <imgui_stdlib.h>                 // IWYU pragma: export
#include <render/engine/opengl/opengl.h>  // IWYU pragma: export
#include <render/render.h>                // IWYU pragma: export
#include <tasks/tasks.h>                  // IWYU pragma: export

#include "utils/random.h" // IWYU pragma: export
#include "utils/utils.h"  // IWYU pragma: export


using RenderType = Render<render::OpenGL>;

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC // NOLINT
#include <crtdbg.h>
#define DEBUG_NEW new (_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBUG_NEW // NOLINT
#endif
