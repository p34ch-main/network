#pragma once

#include "protocol/protocol_tcp.h"
#include "server/server.h"


struct ServerTCP final : CommonServer {
    using Parent   = CommonServer;
    using Protocol = TCP;

    ServerTCP(const std::string& ip, asio::ip::port_type port)
      : Parent(ip, port), m_acceptor(asio::ip::tcp::acceptor(m_io_context, {m_address, m_port})) {
        connect<SIGNAL::ON_DISCONNECT>([this](const ProtocolPtr& protocol) { std::erase(m_connections, protocol); });

        m_acceptor.set_option(asio::ip::tcp::acceptor::reuse_address(true));
    }

    void setActiveConnection(ProtocolWPtr connection) { m_active_connection = std::move(connection); }

    void run() override {
        startAccept();
        Parent::run();
    };

    void stop() override {
        std::lock_guard lock(m_mutex);

        asio::post(m_acceptor.get_executor(), [this]() { m_acceptor.cancel(); });
        Parent::stop();
    }

private:
    void startAccept() {
        auto connection = std::make_shared<Protocol>(m_io_context, *m_callbacks);

        auto& socket = connection->socket();
        m_acceptor.async_accept(socket, [this, connection = std::move(connection)](const asio::error_code& error) {
            if (!error) {
                m_connections.emplace_back(connection)->start();
                connection->notify();
            } else {
                spdlog::critical(
                  "{} server on port {}. Error {}: {}", name<Protocol>(), m_port, error.value(), error.message());
            }
            startAccept();
        });
    }

    void stopConnections() override {
        while (!m_connections.empty()) {
            m_connections.front()->stop();
        }
    }

private:
    asio::ip::tcp::acceptor m_acceptor;
};