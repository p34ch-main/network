#pragma once

#include "common/base.h"


struct CommonServer : CommonBase {
    using Parent = CommonBase;
    using CommonBase::CommonBase;

    void send(const std::string& msg) override {
        if (auto ptr = m_active_connection.lock(); ptr) {
            ptr->send(msg);
        }
    }

    const std::vector<ProtocolPtr>& connections() const noexcept { return m_connections; }

protected:
    std::vector<ProtocolPtr> m_connections;
    ProtocolWPtr             m_active_connection;
};
