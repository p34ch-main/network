#pragma once

#include "protocol/protocol_udp.h"
#include "server/server.h"


struct ServerUDP final : CommonServer {
    using Parent       = CommonServer;
    using Protocol     = UDP;
    using Endpoint     = Protocol::Endpoint;
    using EndpointPtr  = std::shared_ptr<Endpoint>;
    using EndpointWPtr = EndpointPtr::weak_type;

    ServerUDP(const std::string& ip, asio::ip::port_type port) : Parent(ip, port) {
        m_connections.emplace_back(std::make_shared<Protocol>(m_io_context, m_address, m_port, *m_callbacks))->start();
        m_active_connection = m_connections.back();

        connect<SIGNAL::ON_READ_END>([this](const ProtocolPtr& protocol) {
            auto udp = std::dynamic_pointer_cast<Protocol>(protocol);
            addEndpoint(udp->endpoints().second);
        });
    }

    const std::vector<EndpointPtr>& endpoints() const noexcept { return m_remote_endpoints; }

    void addEndpoint(const Endpoint& ep) {
        std::lock_guard lock(m_mutex);

        auto position = std::find_if(m_remote_endpoints.begin(),
                                     m_remote_endpoints.end(),
                                     [&ep](const EndpointPtr& endpoint) { return ep == *endpoint; });

        if (position == m_remote_endpoints.end()) {
            m_remote_endpoints.emplace_back(std::make_shared<Endpoint>(ep));
        }
    }

    void setActiveEndpoint(const EndpointWPtr& ep) {
        if (auto endpoint = ep.lock()) {
            auto udp = std::dynamic_pointer_cast<Protocol>(m_connections.back());
            udp->setDestination(*endpoint);
        }
    }

    template<SIGNAL signal>
    void connect(ProtocolBase::Callback&& f) {
        Parent::connect<signal>(std::move(f));
    }

private:
    void stopConnections() override { m_connections.back()->stop(); }

private:
    std::vector<EndpointPtr> m_remote_endpoints;
};

// UDP doesn't have connections
template<>
void ServerUDP::connect<SIGNAL::ON_CONNECT>(ProtocolBase::Callback&& f) = delete;

template<>
void ServerUDP::connect<SIGNAL::ON_DISCONNECT>(ProtocolBase::Callback&& f) = delete;


using Endpoint     = ServerUDP::Endpoint;
using EndpointPtr  = ServerUDP::EndpointPtr;
using EndpointWPtr = ServerUDP::EndpointWPtr;
