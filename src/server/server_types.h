#pragma once

#include "server/server_tcp.h"
#include "server/server_udp.h"


namespace detail
{

template<typename T>
struct ServerTypeMapper;

template<>
struct ServerTypeMapper<TCP> {
    using Type = ServerTCP;
};

template<>
struct ServerTypeMapper<UDP> {
    using Type = ServerUDP;
};

} // namespace detail


template<class T>
concept server = std::is_base_of_v<CommonServer, T>;

template<typename T>
using Server = detail::ServerTypeMapper<T>::Type;

using CommonServerPtr = std::shared_ptr<CommonServer>;
