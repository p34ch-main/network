#pragma once

#include "protocol/protocol.h"


struct Callbacks {
    Callbacks() : m_callbacks(std::make_unique<ProtocolBase::Callbacks>()) {}

    template<SIGNAL signal>
    void connect(ProtocolBase::Callback&& f) {
        static_assert(signal < SIGNAL::COUNT);
        (*m_callbacks)[signal].emplace_back(std::move(f));
    }

protected:
    const std::unique_ptr<ProtocolBase::Callbacks> m_callbacks;
};

struct CommonBase : Callbacks, NoCopyNoMove {
    CommonBase(asio::ip::port_type port) : m_port(port) {}
    CommonBase(asio::ip::address address, asio::ip::port_type port) : m_address(std::move(address)), m_port(port) {}
    CommonBase(const std::string& address, asio::ip::port_type port)
      : m_address(asio::ip::address::from_string(address)), m_port(port) {}

    virtual ~CommonBase() noexcept = default;

    const auto& port() const noexcept { return m_port; }
    const auto& address() const noexcept { return m_address; }

    virtual void run() {
        assert(std::this_thread::get_id() != renderThreadId() && "you cannot run the Entity in the main thread");

        m_io_context.run();
    }

    virtual bool stopped() const noexcept { return m_io_context.stopped(); }
    virtual void stop() {
        std::lock_guard lock(m_mutex);
        if (!m_io_context.stopped()) {
            stopConnections();
            m_io_context.stop();
        }
    }

    virtual void send(const std::string& msg) = 0;

protected:
    virtual void stopConnections() {}

protected:
    const asio::ip::address   m_address;
    const asio::ip::port_type m_port;
    std::recursive_mutex      m_mutex;
    asio::io_context          m_io_context;
};

template<class T>
concept common_base = std::is_base_of_v<CommonBase, T>;

template<template<typename> typename... T>
requires(sizeof...(T) > 0)
struct Entities {};


using CommonBasePtr = std::shared_ptr<CommonBase>;
