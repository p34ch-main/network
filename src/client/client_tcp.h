#pragma once

#include "protocol/protocol_tcp.h"
#include "client/client.h"


struct ClientTCP final : CommonClient {
    using Parent   = CommonClient;
    using Protocol = TCP;

    ClientTCP(const std::string& ip, asio::ip::port_type port) : Parent(ip, port) {
        auto ptr = std::make_shared<Protocol>(m_io_context, *m_callbacks);

        asio::ip::tcp::socket& socket = ptr->socket();
        socket.connect({m_address, m_port});
        m_connection = std::move(ptr);
    }

    void run() override {
        m_connection->start();
        std::dynamic_pointer_cast<Protocol>(m_connection)->notify();
        Parent::run();
    };
};
