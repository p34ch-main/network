#pragma once

#include "protocol/protocol_udp.h"
#include "client/client.h"


struct ClientUDP final : CommonClient {
    using Parent   = CommonClient;
    using Protocol = UDP;

    ClientUDP(const std::string& ip, asio::ip::port_type dst_port, asio::ip::port_type src_port = 0)
      : Parent(ip, dst_port) {
        if (!src_port) {
            src_port = dice(49152, 65535);
            spdlog::info("Assigned UDP src port: {}", src_port);
        }

        auto ptr =
          std::make_shared<Protocol>(m_io_context, asio::ip::address::from_string("0.0.0.0"), src_port, *m_callbacks);

        ptr->setDestination(m_address, m_port);
        ptr->start();

        m_connection = std::move(ptr);
    }
};