#pragma once

#include "common/base.h"


struct CommonClient : CommonBase {
    using Parent = CommonBase;
    using CommonBase::CommonBase;

    void send(const std::string& msg) override {
        if (m_connection) {
            m_connection->send(msg);
        }
    }

protected:
    void stopConnections() override { m_connection->stop(); }

protected:
    ProtocolPtr m_connection;
};
