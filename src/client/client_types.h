#pragma once

#include "client/client_tcp.h"
#include "client/client_udp.h"


namespace detail
{

template<typename T>
struct ClientTypeMapper;

template<>
struct ClientTypeMapper<TCP> {
    using Type = ClientTCP;
};

template<>
struct ClientTypeMapper<UDP> {
    using Type = ClientUDP;
};

} // namespace detail


template<class T>
concept client = std::is_base_of_v<CommonClient, T>;

template<typename T>
using Client = detail::ClientTypeMapper<T>::Type;

using CommonClientPtr = std::shared_ptr<CommonClient>;
