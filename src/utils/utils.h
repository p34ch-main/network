#pragma once

#include <chrono>

#if __clang__
#define OFFSET_LEFT 29
#define OFFSET_RIGHT 1
#elif _MSC_VER
#define __PRETTY_FUNCTION__ __FUNCSIG__ // NOLINT
#define OFFSET_LEFT 86
#define OFFSET_RIGHT 7
#elif defined(__GNUC__) || defined(__GNUG__)
#define OFFSET_LEFT 44
#define OFFSET_RIGHT 50
#endif

struct NoCopy {
    NoCopy()                             = default;
    NoCopy(const NoCopy&)                = delete;
    NoCopy& operator=(const NoCopy&)     = delete;
    NoCopy(NoCopy&&) noexcept            = default;
    NoCopy& operator=(NoCopy&&) noexcept = default;
};

struct NoCopyNoMove {
    NoCopyNoMove()                                   = default;
    NoCopyNoMove(const NoCopyNoMove&)                = delete;
    NoCopyNoMove& operator=(const NoCopyNoMove&)     = delete;
    NoCopyNoMove(NoCopyNoMove&&) noexcept            = delete;
    NoCopyNoMove& operator=(NoCopyNoMove&&) noexcept = delete;
};

struct Packet final {
    std::chrono::system_clock::time_point time;
    std::string                           src;
    std::string                           dst;
    std::vector<char>                     msg;
    bool                                  direction;
};


template<typename T>
consteval std::string_view name() {
    std::string_view name(__PRETTY_FUNCTION__);
    return std::string_view{name.begin() + OFFSET_LEFT, name.end() - OFFSET_RIGHT};
}

template<typename T>
constexpr std::string nameStr() {
    std::string string_name(name<T>());
    return string_name;
}

template<typename T>
consteval std::string_view nameTemplated() {
    std::string_view full_name(name<T>());
    auto             start = full_name.find_first_of('<') + 1;
    auto             end   = full_name.find_first_of('>');
    return std::string_view{full_name.begin() + start, full_name.begin() + end};
}


template<typename T>
constexpr std::string nameTemplatedStr() {
    std::string string_name(nameTemplated<T>());
    return string_name;
}
