#pragma once

namespace common
{

inline void showLogTable(const std::string& name, const std::vector<Packet>& messages, bool& show) {
    constexpr ImGuiTableFlags flags      = ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg;
    std::string               table_name = (name + "(log)");

    ImGui::Begin(table_name.c_str(), &show);

    if (ImGui::BeginTable(table_name.c_str(), 5, flags)) {
        ImGui::TableSetupColumn("Type", ImGuiTableColumnFlags_WidthFixed, 0.0f);
        ImGui::TableSetupColumn("Time", ImGuiTableColumnFlags_WidthFixed, 0.0f);
        ImGui::TableSetupColumn("Src", ImGuiTableColumnFlags_WidthFixed, 0.0f);
        ImGui::TableSetupColumn("Dest", ImGuiTableColumnFlags_WidthFixed, 0.0f);
        ImGui::TableSetupColumn("Message");
        ImGui::TableHeadersRow();

        for (const auto& [time, src, dst, msg, dir] : messages) {
            ImGui::TableNextRow();

            ImGui::TableNextColumn();
            ImGui::Text(dir ? "Write" : "Read");

            ImGui::TableNextColumn();
            const auto& now = std::chrono::current_zone()->to_local(time);
            ImGui::Text("%s", std::format("{0:%F %T}", now).c_str());

            ImGui::TableNextColumn();
            ImGui::Text("%s", src.c_str());

            ImGui::TableNextColumn();
            ImGui::Text("%s", dst.c_str());

            ImGui::TableNextColumn();
            ImGui::Text("%s", msg.data());
        }
        ImGui::EndTable();
    }

    ImGui::End();
}

} // namespace common