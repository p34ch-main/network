#pragma once

#include "common/base.h"

struct GUIBase : NoCopyNoMove {
    GUIBase(std::string name = "") : m_name(std::move(name)) {};
    virtual ~GUIBase() = default;

    virtual bool isActive() const noexcept { return m_show_window; }
    virtual void window() = 0;

    CommonBasePtr& entity() noexcept { return m_entity; }

protected:
    virtual void connect()    = 0;
    virtual void disconnect() = 0;

protected:
    CommonBasePtr        m_entity;
    std::future<void>    m_future;
    std::recursive_mutex m_mutex;

    std::string m_name;
    std::string m_address = "0.0.0.0";
    int         m_port    = 5000;

    bool m_show_window = true;
};

struct ConfigurationInterface {
    virtual ~ConfigurationInterface() = default;
    virtual void window() {};
    virtual void connect() {};
    virtual void disconnect() {};
};

template<template<template<typename> typename, typename> typename GUIType,
         template<typename>
         typename EntityType,
         protocol ProtocolType>
struct Configuration : ConfigurationInterface {
    using Base = GUIType<EntityType, ProtocolType>;
    Configuration(Base& /*unused*/) {}
};

template<template<template<typename> typename, typename> typename... T>
requires(sizeof...(T) > 0)
struct GUIs {};

using GUIBasePtr = std::shared_ptr<GUIBase>;
