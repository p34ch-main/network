
#include "ui/gui_fabric.h"
#include "client/client_types.h"
#include "ui/gui_echo.h"
#include "ui/gui_logger.h"

template<template<typename> typename EntityType,
         typename ProtocolType,
         template<template<typename> typename, typename>
         typename GUIType>
requires common_base<EntityType<ProtocolType>>
static inline void populate(std::vector<GUIBasePtr>& gui) {
    static std::size_t id = 0;

    // imgui cannot work with strig_view
    const std::string& entity_name   = nameTemplatedStr<Entities<EntityType>>();
    const std::string& protocol_name = nameStr<ProtocolType>();
    const std::string& gui_name      = nameTemplatedStr<GUIs<GUIType>>();

    if (ImGui::ScopedBeginMenu(entity_name.c_str())) {
        if (ImGui::ScopedBeginMenu(protocol_name.c_str())) {
            if (ImGui::MenuItem(gui_name.c_str())) {
                gui.emplace_back(std::make_shared<GUIType<EntityType, ProtocolType>>(
                  gui_name + " - " + entity_name + " - " + protocol_name + "##" + std::to_string(id++)));
            }
        }
    }
}

template<typename... T>
struct Menu;

template<template<typename> typename EntityType,
         typename ProtocolType,
         template<template<typename> typename, typename>
         typename GUIType>
struct Menu<Entities<EntityType>, Protocols<ProtocolType>, GUIs<GUIType>> {
    static void generate(std::vector<GUIBasePtr>& gui) { populate<EntityType, ProtocolType, GUIType>(gui); }
};

template<template<typename> typename EntityType,
         typename ProtocolType,
         template<template<typename> typename, typename>
         typename... GUITypes>
struct Menu<Entities<EntityType>, Protocols<ProtocolType>, GUIs<GUITypes...>> {
    static void generate(std::vector<GUIBasePtr>& gui) {
        (Menu<Entities<EntityType>, Protocols<ProtocolType>, GUIs<GUITypes>>::generate(gui), ...);
    }
};

template<template<typename> typename EntityType,
         typename... ProtocolTypes,
         template<template<typename> typename, typename>
         typename... GUITypes>
struct Menu<Entities<EntityType>, Protocols<ProtocolTypes...>, GUIs<GUITypes...>> {
    static void generate(std::vector<GUIBasePtr>& gui) {
        (Menu<Entities<EntityType>, Protocols<ProtocolTypes>, GUIs<GUITypes...>>::generate(gui), ...);
    }
};

template<template<typename> typename... EntityTypes,
         typename... ProtocolTypes,
         template<template<typename> typename, typename>
         typename... GUITypes>
struct Menu<Entities<EntityTypes...>, Protocols<ProtocolTypes...>, GUIs<GUITypes...>> {
    static void generate(std::vector<GUIBasePtr>& gui) {
        (Menu<Entities<EntityTypes>, Protocols<ProtocolTypes...>, GUIs<GUITypes...>>::generate(gui), ...);
    }
};


ServerFabric::ServerFabric() {
    RENDER_ADD_MEMBER_WINDOW(window);
}

void ServerFabric::window() {
    if (ImGui::ScopedBeginMainMenuBar()) {
        Menu<Entities<Server, Client>, Protocols<TCP, UDP>, GUIs<Simple, Log>>::generate(m_GUIs);

        // only server funcs
        Menu<Entities<Server>, Protocols<TCP, UDP>, GUIs<Echo>>::generate(m_GUIs);
    }

    for (auto& gui : m_GUIs) {
        gui->window();
    }
    std::erase_if(m_GUIs, [](const GUIBasePtr& window) { return !window->isActive(); });
}
