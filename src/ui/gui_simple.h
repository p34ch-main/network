#pragma once

#include "common/base.h"
#include "protocol/protocol.h"
#include "server/server_types.h"
#include "client/client_types.h"
#include "ui/gui_base.h"


template<template<typename> typename EntityType, protocol ProtocolType>
requires common_base<EntityType<ProtocolType>>
struct Simple;

// -------------------------------------

template<>
struct Configuration<Simple, Server, TCP> : ConfigurationInterface {
    using Base    = Simple<Server, TCP>;
    using Entity  = Server<TCP>;
    using Clients = std::vector<ProtocolWPtr>;

    Configuration(Base& base);
    void window() override;
    void connect() override;

private:
    Base&              m_base;
    Clients            m_clients;
    Clients::size_type m_selected_client = 0;
};

template<>
struct Configuration<Simple, Server, UDP> : ConfigurationInterface {
    using Base    = Simple<Server, UDP>;
    using Entity  = Server<UDP>;
    using Clients = std::vector<EndpointWPtr>;

    Configuration(Base& base);
    void window() override;
    void connect() override;

private:
    Base&              m_base;
    Clients            m_clients;
    Clients::size_type m_selected_client = 0;
};

template<>
struct Configuration<Simple, Client, TCP> : ConfigurationInterface {
    using Base = Simple<Client, TCP>;

    Configuration(Base& base);
    void window() override;
    void connect() override;

private:
    Base& m_base;
};

template<>
struct Configuration<Simple, Client, UDP> : ConfigurationInterface {
    using Base = Simple<Client, UDP>;

    Configuration(Base& base);
    void window() override;

private:
    Base& m_base;
};

// ------------------------------------- GUI

template<template<typename> typename EntityType, protocol ProtocolType>
requires common_base<EntityType<ProtocolType>>
struct Simple : GUIBase {
    using Entity   = EntityType<ProtocolType>;
    using Protocol = ProtocolType;
    using Parent   = GUIBase;
    using Config   = Configuration<Simple, EntityType, ProtocolType>;
    friend Config;


    Simple(const std::string& name = "") : Parent(name), m_config(*this) {
        spdlog::info("Create: {}", m_name);
        spdlog::info("\tProtocol: {}", protocol_name);
    }

    Simple(Simple&&) noexcept            = default;
    Simple& operator=(Simple&&) noexcept = default;

    ~Simple() override {
        disconnect();
        spdlog::info("Destroy: {}", m_name);
        spdlog::info("\tProtocol: {}", protocol_name);
    }

    void window() override {
        std::lock_guard lock(m_mutex);

        if (m_show_window) {
            ImGui::ScopedBegin(m_name.c_str(), &m_show_window);
            m_config.window();
        }
    }

protected:
    void connect() override {
        try {
            spdlog::info("Start: {}", m_name);
            spdlog::info("\tProtocol: {}", protocol_name);
            spdlog::info("\tPort: {}", m_port);

            m_entity = std::make_shared<Entity>(m_address, m_port);
            m_config.connect();

            m_future = TASKS_START([](const CommonBasePtr& s) { s->run(); }, m_entity);

        } catch (std::exception& e) { spdlog::critical("{}", e.what()); }
    }

    void disconnect() final {
        if (m_entity) {
            m_config.disconnect();

            m_entity->stop();
            m_future.wait();
            m_entity.reset();

            spdlog::info("Stop: {}", m_name);
            spdlog::info("\tProtocol: {}", protocol_name);
            spdlog::info("\tPort: {}", m_port);
        }
    }

private:
    Config m_config;
    static_assert(std::is_base_of_v<ConfigurationInterface, Config>,
                  "Configuration should be derived from ConfigurationInterface");

protected:
    static constexpr std::string_view protocol_name = name<Protocol>();
};

// ------------------------------------- SERVER --- TCP

inline Configuration<Simple, Server, TCP>::Configuration(Base& base) : m_base(base) {
    std::ignore = m_base.m_show_window;
}

inline void Configuration<Simple, Server, TCP>::window() {
    {
        ImGui::ScopedBeginDisabled(!!m_base.m_entity);
        ImGui::DragInt("Port", &m_base.m_port, 1, 0, 65535);
    }

    if (ImGui::Button(m_base.m_entity ? "Stop" : "Start", ImVec2(120, 0))) {
        m_base.m_entity ? m_base.disconnect() : m_base.connect();
    }

    if (m_base.m_entity) {
        ImGui::Text("Clients:");
        if (ImGui::ScopedBeginListBox("##clients", ImVec2(-1, -1))) {
            auto last_val = m_selected_client;
            for (Clients::size_type n = 0; n < m_clients.size(); n++) {
                const bool is_selected = m_selected_client == n;

                if (auto ptr = m_clients[n].lock()) {
                    if (ImGui::Selectable(ptr->toString().second.c_str(), is_selected)) {
                        m_selected_client = n;
                    }
                }

                if (is_selected) {
                    ImGui::SetItemDefaultFocus();
                }
            }

            if (m_selected_client >= m_clients.size()) {
                m_selected_client = std::numeric_limits<Clients::size_type>::max();
            }

            if (last_val != m_selected_client && m_selected_client != std::numeric_limits<Clients::size_type>::max()) {
                auto entity = std::dynamic_pointer_cast<Entity>(m_base.m_entity);
                entity->setActiveConnection(m_clients[m_selected_client]);
            }
        }
    }
}

inline void Configuration<Simple, Server, TCP>::connect() {
    m_base.m_entity->connect<SIGNAL::ON_CONNECT>([this](const ProtocolPtr& ptr) {
        std::lock_guard lock(m_base.m_mutex);
        m_clients.emplace_back(ptr);
        spdlog::info("new connection");
    });

    m_base.m_entity->connect<SIGNAL::ON_DISCONNECT>([this](const ProtocolPtr& ptr) {
        std::lock_guard lock(m_base.m_mutex);
        spdlog::info("close connection");
        std::erase_if(m_clients, [ptr](const ProtocolWPtr& client) {
            return !ptr.owner_before(client) && !client.owner_before(ptr);
        });
    });
}

// ------------------------------------- SERVER --- UDP

inline Configuration<Simple, Server, UDP>::Configuration(Base& base) : m_base(base) {
    std::ignore = m_base.m_show_window;
}

inline void Configuration<Simple, Server, UDP>::window() {
    {
        ImGui::ScopedBeginDisabled(!!m_base.m_entity);
        ImGui::DragInt("Port", &m_base.m_port, 1, 0, 65535);
    }

    if (ImGui::Button(m_base.m_entity ? "Stop" : "Start", ImVec2(120, 0))) {
        m_base.m_entity ? m_base.disconnect() : m_base.connect();
    }

    if (m_base.m_entity) {
        ImGui::Text("Clients:");
        if (ImGui::ScopedBeginListBox("##clients", ImVec2(-1, -1))) {
            auto last_val = m_selected_client;
            for (Clients::size_type n = 0; n < m_clients.size(); n++) {
                const bool is_selected = m_selected_client == n;

                if (auto ptr = m_clients[n].lock()) {
                    if (ImGui::Selectable(endpointToString(*ptr).c_str(), is_selected)) {
                        m_selected_client = n;
                    }
                }

                if (is_selected) {
                    ImGui::SetItemDefaultFocus();
                }
            }

            if (m_selected_client >= m_clients.size()) {
                m_selected_client = std::numeric_limits<Clients::size_type>::max();
            }

            if (last_val != m_selected_client && m_selected_client != std::numeric_limits<Clients::size_type>::max()) {
                auto entity = std::dynamic_pointer_cast<Entity>(m_base.m_entity);
                entity->setActiveEndpoint(m_clients[m_selected_client]);
            }
        }
    }
}

inline void Configuration<Simple, Server, UDP>::connect() {
    m_base.m_entity->connect<SIGNAL::ON_READ_END>([this](const ProtocolPtr&) {
        std::lock_guard lock(m_base.m_mutex);
        m_clients.clear();

        auto        entity    = std::dynamic_pointer_cast<Entity>(m_base.m_entity);
        const auto& endpoints = entity->endpoints();
        std::copy(endpoints.begin(), endpoints.end(), std::back_inserter(m_clients));
    });
}

// ------------------------------------- CLIENT --- TCP

inline Configuration<Simple, Client, TCP>::Configuration(Base& base) : m_base(base) {
    m_base.m_address = "127.0.0.1";
}

inline void Configuration<Simple, Client, TCP>::window() {
    {
        ImGui::ScopedBeginDisabled(!!m_base.m_entity);
        ImGui::InputText("IP", &m_base.m_address);
        ImGui::DragInt("Port", &m_base.m_port, 1, 0, 65535);
    }

    if (ImGui::Button(m_base.m_entity ? "Stop" : "Start", ImVec2(120, 0))) {
        m_base.m_entity ? m_base.disconnect() : m_base.connect();
    }
}

inline void Configuration<Simple, Client, TCP>::connect() {
    m_base.m_entity->connect<SIGNAL::ON_CONNECT>([](const ProtocolPtr&) { spdlog::info("new connection"); });

    m_base.m_entity->connect<SIGNAL::ON_DISCONNECT>([this](const ProtocolPtr&) {
        m_base.m_entity.reset();

        spdlog::info("Was stopped outside: {}", m_base.m_name);
        spdlog::info("\tProtocol: {}", name<TCP>());
        spdlog::info("\tPort: {}", m_base.m_port);
    });
}

// ------------------------------------- CLIENT --- UDP

inline Configuration<Simple, Client, UDP>::Configuration(Base& base) : m_base(base) {
    m_base.m_address = "127.0.0.1";
}

inline void Configuration<Simple, Client, UDP>::window() {
    {
        ImGui::ScopedBeginDisabled(!!m_base.m_entity);
        ImGui::InputText("IP", &m_base.m_address);
        ImGui::DragInt("Port", &m_base.m_port, 1, 0, 65535);
    }

    if (ImGui::Button(m_base.m_entity ? "Stop" : "Start", ImVec2(120, 0))) {
        m_base.m_entity ? m_base.disconnect() : m_base.connect();
    }
}
