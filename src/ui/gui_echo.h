#pragma once

#include "ui/gui_logger.h"


template<template<typename> typename EntityType, protocol ProtocolType>
requires common_base<EntityType<ProtocolType>>
struct Echo;

// -------------------------------------

template<>
struct Configuration<Echo, Server, TCP> : ConfigurationInterface {
    using Base   = Echo<Server, TCP>;
    using Entity = Server<TCP>;
    Configuration(Base& base) : m_base(base) {}
    void connect() override;

private:
    Base& m_base;
};

template<>
struct Configuration<Echo, Server, UDP> : ConfigurationInterface {
    using Base   = Echo<Server, UDP>;
    using Entity = Server<UDP>;
    Configuration(Base& base) : m_base(base) {}
    void connect() override;

private:
    Base& m_base;
};

// -------------------------------------

template<template<typename> typename EntityType, protocol ProtocolType>
requires common_base<EntityType<ProtocolType>>
struct Echo final : Log<EntityType, ProtocolType> {
    using Entity   = EntityType<ProtocolType>;
    using Protocol = ProtocolType;
    using Parent   = Log<EntityType, ProtocolType>;
    using Config   = Configuration<Echo, EntityType, ProtocolType>;
    friend Config;

    Echo(const std::string& name = "") : Parent(name), m_config(*this) {}
    Echo(Echo&&) noexcept            = default;
    Echo& operator=(Echo&&) noexcept = default;

protected:
    void connect() override {
        Parent::connect();

        auto entity = std::dynamic_pointer_cast<Entity>(this->m_entity);
        if (!entity) {
            return;
        }

        m_config.connect();
    }

private:
    Config m_config;
    static_assert(std::is_base_of_v<ConfigurationInterface, Config>,
                  "Configuration should be derived from ConfigurationInterface");
};

// -------------------------------------

inline void Configuration<Echo, Server, TCP>::connect() {
    m_base.m_entity->connect<SIGNAL::ON_READ_END>(
      [](const ProtocolPtr& srv) { srv->send(srv->lastMessage().msg.data()); });
}

inline void Configuration<Echo, Server, UDP>::connect() {
    m_base.m_entity->connect<SIGNAL::ON_READ_END>(
      [](const ProtocolPtr& srv) { srv->send(srv->lastMessage().msg.data()); });
}