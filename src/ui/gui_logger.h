#pragma once

#include "ui/gui_common.h"
#include "ui/gui_simple.h"


template<template<typename> typename EntityType, protocol ProtocolType>
requires common_base<EntityType<ProtocolType>>
struct Log : Simple<EntityType, ProtocolType> {
    using Entity   = EntityType<ProtocolType>;
    using Protocol = ProtocolType;
    using Parent   = Simple<EntityType, ProtocolType>;

    Log(const std::string& name = "") : Parent(name) {}
    Log(Log&&) noexcept            = default;
    Log& operator=(Log&&) noexcept = default;

    void window() override {
        std::lock_guard lock(this->m_mutex);

        Parent::window();
        if (this->m_entity) {
            common::showLogTable(this->m_name, m_messages, m_show_info);

            ImGui::Begin(std::string(this->m_name + "(Send message)").data(), &m_show_info);

            if (ImGui::Button("Send")) {
                this->m_entity->send(m_message + (m_add_cr ? "\n" : "") + (m_add_lf ? "\r" : ""));
            }

            ImGui::SameLine();
            if (ImGui::Button("Clear")) {
                m_message.clear();
            }

            ImGui::SameLine();
            ImGui::Checkbox("CR", &m_add_cr);

            ImGui::SameLine();
            ImGui::Checkbox("LF", &m_add_lf);

            ImGui::Text("Message:");
            ImGui::InputTextMultiline("##send message", &m_message, ImVec2(-1, -1));

            ImGui::End();

            if (!m_show_info) {
                Parent::disconnect();
            }
        }
    }

protected:
    void connect() override {
        Parent::connect();

        auto entity = std::dynamic_pointer_cast<Entity>(this->m_entity);
        if (!entity) {
            return;
        }

        entity->template connect<SIGNAL::ON_READ_END>([this](const ProtocolPtr& ptr) {
            std::lock_guard lock(this->m_mutex);

            m_messages.emplace_back(ptr->lastMessage());
            spdlog::debug("Incoming message");
        });

        entity->template connect<SIGNAL::ON_WRITE_END>([this](const ProtocolPtr& ptr) {
            std::lock_guard lock(this->m_mutex);

            m_messages.emplace_back(ptr->lastMessage());
            spdlog::debug("Outgoing message");
        });

        m_show_info = true;
    }

private:
    std::vector<Packet> m_messages;
    std::string         m_message;
    bool                m_show_info = false;
    bool                m_add_cr    = false;
    bool                m_add_lf    = false;
};
