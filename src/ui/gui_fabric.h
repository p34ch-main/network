#pragma once

#include "ui/gui_base.h"

struct ServerFabric final {
    ServerFabric();

    void window();

private:
    std::vector<GUIBasePtr> m_GUIs;
};