#pragma once

#include "protocol.h"


struct TCP final : ProtocolBase {
    using Parent = ProtocolBase;

    TCP(asio::io_context& context, Callbacks& callbacks);

    asio::ip::tcp::socket&       socket() noexcept { return m_socket; }
    const asio::ip::tcp::socket& socket() const noexcept { return m_socket; }
    void                         send(const std::string& msg) override;

    void start() override;
    void stop() override;

    void                                notify();
    std::pair<std::string, std::string> toString() const override {
        std::lock_guard lock(m_mutex);
        return {endpointToString(m_socket.local_endpoint()), endpointToString(m_socket.remote_endpoint())};
    };

private:
    asio::awaitable<void> reader();
    asio::awaitable<void> writer();

private:
    asio::io_context&     m_io_context;
    asio::ip::tcp::socket m_socket;
    asio::steady_timer    m_timer;
};


template<class T>
concept is_tcp = std::is_same_v<T, TCP>;

using TCPPtr  = std::shared_ptr<TCP>;
using TCPWPtr = TCPPtr::weak_type;
