#pragma once

#include "protocol.h"


struct UDP final : ProtocolBase {
    using Parent   = ProtocolBase;
    using Endpoint = asio::ip::udp::endpoint;


    UDP(asio::io_context& context, const asio::ip::address& ip, asio::ip::port_type port, Callbacks& callbacks);
    ~UDP() override = default;

    void setDestination(const asio::ip::address& ip, asio::ip::port_type port) { m_remote_endpoint = {ip, port}; }
    void setDestination(const Endpoint& ep) { m_remote_endpoint = ep; }

    void send(const std::string& msg) override;

    void                                start() override;
    void                                stop() override;
    std::pair<std::string, std::string> toString() const override {
        std::lock_guard lock(m_mutex);
        return {endpointToString(m_endpoint), endpointToString(m_remote_endpoint)};
    };
    std::pair<Endpoint, Endpoint> endpoints() const {
        std::lock_guard lock(m_mutex);
        return {m_endpoint, m_remote_endpoint};
    };

private:
    void reader();
    void writer();

private:
    Endpoint              m_endpoint;
    Endpoint              m_remote_endpoint;
    asio::ip::udp::socket m_socket;
    std::vector<char>     m_recv_buffer;
    std::string           m_send_buffer;
};


template<class T>
concept is_udp = std::is_same_v<T, UDP>;

using UDPPtr  = std::shared_ptr<UDP>;
using UDPWPtr = UDPPtr::weak_type;
