#pragma once


enum SIGNAL {
    ON_READ_END,
    ON_WRITE_END,
    ON_CONNECT,
    ON_DISCONNECT,

    COUNT,
};


template<class Endpoint>
inline std::string endpointToString(const Endpoint& ep) {
    return {ep.address().to_string() + ":" + std::to_string(ep.port())};
}

inline void copyMsg(std::vector<char>& dst, std::span<const char> src) {
    dst.resize(src.size() + 1);
    std::memcpy(dst.data(), src.data(), src.size());
    dst[src.size()] = 0;
}


template<class Endpoint>
inline Packet buildPacket(const Endpoint& src, const Endpoint& dst, bool direction, std::span<char> data) {
    Packet packet;
    packet.time      = std::chrono::system_clock::now();
    packet.src       = endpointToString(src);
    packet.dst       = endpointToString(dst);
    packet.direction = direction;
    copyMsg(packet.msg, data);
    return packet;
}

struct ProtocolBase : public std::enable_shared_from_this<ProtocolBase> {
    using ProtocolPtr  = std::shared_ptr<ProtocolBase>;
    using ProtocolWPtr = ProtocolPtr::weak_type;
    using Callback     = std::function<void(ProtocolPtr)>;
    using Callbacks    = std::array<std::vector<Callback>, SIGNAL::COUNT>;

    ProtocolBase(Callbacks& callbacks) : m_last_packet(), m_callbacks(callbacks) {
        spdlog::debug("ProtocolBase was created");
    }
    virtual ~ProtocolBase() noexcept { spdlog::debug("ProtocolBase was destroyed"); };

    virtual void start() = 0;
    virtual void stop()  = 0;

    virtual const Packet&                       lastMessage() const noexcept { return m_last_packet; }
    virtual std::pair<std::string, std::string> toString() const = 0;

    virtual void send(const std::string& msg) {
        std::lock_guard lock(m_mutex);
        m_write_msgs.emplace(msg);
    }

protected:
    mutable std::recursive_mutex m_mutex;
    std::queue<std::string>      m_write_msgs;
    Packet                       m_last_packet;

    Callbacks& m_callbacks;
};


template<class T>
concept protocol = std::is_base_of_v<ProtocolBase, T>;

template<typename... T>
requires(sizeof...(T) > 0)
struct Protocols {};

using ProtocolPtr  = ProtocolBase::ProtocolPtr;
using ProtocolWPtr = ProtocolBase::ProtocolWPtr;
