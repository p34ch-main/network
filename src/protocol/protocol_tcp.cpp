#include "protocol_tcp.h"


TCP::TCP(asio::io_context& context, Callbacks& callbacks)
  : Parent(callbacks), m_io_context(context), m_socket(context), m_timer(m_socket.get_executor()) {
    m_timer.expires_at(std::chrono::steady_clock::time_point::max());
}

void TCP::send(const std::string& msg) {
    Parent::send(msg);
    m_timer.cancel_one();
}

void TCP::start() {
    std::lock_guard lock(m_mutex);
    co_spawn(
      m_socket.get_executor(),
      [self = shared_from_this()] { return std::dynamic_pointer_cast<TCP>(self)->reader(); },
      asio::detached);
    co_spawn(
      m_socket.get_executor(),
      [self = shared_from_this()] { return std::dynamic_pointer_cast<TCP>(self)->writer(); },
      asio::detached);
}

void TCP::notify() {
    std::lock_guard lock(m_mutex);
    for (const auto& f : m_callbacks[SIGNAL::ON_CONNECT]) {
        f(shared_from_this());
    }
}

void TCP::stop() {
    std::lock_guard lock(m_mutex);
    if (m_socket.is_open()) {
        asio::post(m_io_context, [this]() { m_socket.close(); });
        m_socket.shutdown(asio::ip::tcp::socket::shutdown_both);
        m_timer.cancel();

        for (const auto& f : m_callbacks[SIGNAL::ON_DISCONNECT]) {
            f(shared_from_this());
        }
    }
}

asio::awaitable<void> TCP::reader() {
    try {
        std::string msg;
        while (true) {
            co_await asio::async_read_until(m_socket, asio::dynamic_buffer(msg, 1024), "\n", asio::use_awaitable);

            std::lock_guard lock(m_mutex);
            m_last_packet = buildPacket(m_socket.remote_endpoint(), m_socket.local_endpoint(), false, msg);

            spdlog::debug("TCP read message {}", msg);
            for (const auto& f : m_callbacks[SIGNAL::ON_READ_END]) {
                f(shared_from_this());
            }

            msg.clear();
            m_timer.cancel_one();
        }
    } catch (std::exception& e) {
        stop();
        spdlog::critical("{}", e.what());
    }
}

asio::awaitable<void> TCP::writer() {
    try {
        while (m_socket.is_open()) {
            std::unique_lock lock(m_mutex);

            if (m_write_msgs.empty()) {
                lock.unlock();
                asio::error_code ec;
                co_await m_timer.async_wait(redirect_error(asio::use_awaitable, ec));
            } else {
                auto msg = std::move(m_write_msgs.front());
                m_write_msgs.pop();
                lock.unlock();

                co_await asio::async_write(m_socket, asio::buffer(msg), asio::use_awaitable);

                lock.lock();
                m_last_packet = buildPacket(m_socket.local_endpoint(), m_socket.remote_endpoint(), true, msg);

                spdlog::debug("TCP send message {}", msg);
                for (const auto& f : m_callbacks[SIGNAL::ON_WRITE_END]) {
                    f(shared_from_this());
                }
            }
        } //-V1020
    } catch (std::exception& e) {
        stop();
        spdlog::critical("{}", e.what());
    }
}
