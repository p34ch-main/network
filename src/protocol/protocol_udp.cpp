#include "protocol_udp.h"


UDP::UDP(asio::io_context& context, const asio::ip::address& ip, asio::ip::port_type port, Callbacks& callbacks)
  : Parent(callbacks), m_endpoint(ip, port), m_socket(context, m_endpoint) {
    m_recv_buffer.resize(0x10000);
}

void UDP::send(const std::string& msg) {
    Parent::send(msg);
    writer();
}

void UDP::start() {
    std::lock_guard lock(m_mutex);
    reader();
}

void UDP::stop() {
    std::lock_guard lock(m_mutex);
    if (m_socket.is_open()) {
        m_socket.close();
    }
}

void UDP::reader() {
    m_socket.async_receive_from(
      asio::buffer(m_recv_buffer), m_remote_endpoint, [this](const asio::error_code& error, std::size_t bytes) {
          if (error) {
              spdlog::critical(
                "UDP server on port {}. Error {}: {}", m_endpoint.port(), error.value(), error.message());
              return;
          }

          std::lock_guard lock(m_mutex);
          m_last_packet = buildPacket(m_remote_endpoint, m_endpoint, false, m_recv_buffer);

          spdlog::debug("UDP read message {} from {}",
                        std::string(m_recv_buffer.data(), bytes),
                        endpointToString(m_remote_endpoint));

          for (const auto& f : m_callbacks[SIGNAL::ON_READ_END]) {
              f(shared_from_this());
          }
          reader();
      });
}

void UDP::writer() {
    std::lock_guard lock(m_mutex);
    if (!m_write_msgs.empty()) {
        m_send_buffer = std::move(m_write_msgs.front());
        m_write_msgs.pop();

        m_last_packet = buildPacket(m_endpoint, m_remote_endpoint, true, m_send_buffer);

        spdlog::debug("UDP send message {} to ", m_send_buffer, endpointToString(m_remote_endpoint));

        m_socket.async_send_to(
          asio::buffer(m_send_buffer), m_remote_endpoint, [this](const asio::error_code&, std::size_t) {
              for (const auto& f : m_callbacks[SIGNAL::ON_WRITE_END]) {
                  f(shared_from_this());
              }
          });
    }
}