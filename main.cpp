#include "ui/gui_fabric.h"

#if !defined(_DEBUG) && defined(_WIN32)
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup") // hide console window
#endif

#ifdef _DEBUG
namespace
{
struct TzdbLeakFix {
    ~TzdbLeakFix() {
        // https://developercommunity.visualstudio.com/t/std::chrono::current_zone-produces-a/1513362
        // https://github.com/microsoft/STL/issues/2504
        // to prevent the tzdb allocations from being reported as memory leaks
        std::chrono::get_tzdb_list().~tzdb_list();
    }
} dispatch;
} // namespace
#endif

struct SingletonsHolder {
    RENDER_TYPE render;
};


int main(int /*unused*/, char** /*unused*/) {
#ifdef _DEBUG
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
    spdlog::set_level(spdlog::level::debug); // Set global log level to debug
#endif

    auto render_holder = std::make_unique<SingletonsHolder>();

    RENDER.setup({.window_title = "Window", .style = RenderSettings::Style::CUSTOM});
    ServerFabric server;
    RENDER.exec();

    return 0;
}
