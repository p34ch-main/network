#ifndef VERSION_H
#define VERSION_H

#define VER_FILEVERSION 1, 3, 0, 0
#define VER_FILEVERSION_STR "1.3.0.0\0"

#define VER_PRODUCTVERSION 1, 3, 0, 0
#define VER_PRODUCTVERSION_STR "1.3\0"

#define VER_COMPANYNAME_STR "P34ch"
#define VER_FILEDESCRIPTION_STR "Network"
#define VER_INTERNALNAME_STR "Network"
#define VER_LEGALCOPYRIGHT_STR "https://gitlab.com/p34ch-main/network"
#define VER_LEGALTRADEMARKS1_STR "Apache License"
#define VER_LEGALTRADEMARKS2_STR VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR "Network.exe"
#define VER_PRODUCTNAME_STR "Network"

#define VER_COMPANYDOMAIN_STR "https://gitlab.com/p34ch-main/network"

#endif // VERSION_H
